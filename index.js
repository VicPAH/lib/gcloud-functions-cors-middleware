const cors = require('cors');

const { fromExpress } = require('@VicPAH/gcloud-functions-express-js-middleware-adapter');

const ALLOWED_ORIGINS = [
  'http://localhost:3000',
  'http://192.168.178.32:3000',
  'https://vicpah.gitlab.io',
  'https://vicpah.com.au',
  'https://www.vicpah.com.au',
  'https://vicpah.org.au',
  'https://www.vicpah.org.au',
];

exports.sendCorsHeaders = fromExpress(
  cors({
    origin: ALLOWED_ORIGINS,
    methods: ['GET', 'PATCH', 'POST', 'PUT', 'DELETE', 'OPTIONS', 'HEAD'],
    allowedHeaders: ['Authorization', 'Content-Length', 'Content-Type'],
    credentials: true,
  }),
);
